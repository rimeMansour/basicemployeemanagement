//
//  basicEmployeeApp.swift
//  basicEmployee
//
//  Created by Rime on 16/05/2023.
//

import SwiftUI

@main
struct basicEmployeeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
