//
//  ContentView.swift
//  basicEmployee
//
//  Created by Rime on 16/05/2023.
//

import SwiftUI

struct Entry: Identifiable {
    let id = UUID()
    let name : String
    let email : String
    let jobPosition : String
}

struct ContentView : View {
    @State private var name = ""
    @State private var email = ""
    @State private var jobPosition = ""
    @State private var entryList = [Entry]()
    
    let JobPositions = ["manager","teacher","dancer"]
    
    var body : some View {
        
        VStack {
            
            List{
                ForEach(entryList) { list in
                    VStack{
                        Text(list.name)
                        Text(list.email)
                        Text(list.jobPosition)
                    }
                }
                
            }
            .ignoresSafeArea()
            
            Spacer()
            
            TextField ("Name", text: $name)
            TextField ("Email", text: $email)
            Picker ("Job Position", selection: $jobPosition) {
                ForEach(JobPositions, id: \.self) {
                    position in
                    Text(position)
                }
                
            }
            .pickerStyle(MenuPickerStyle())
            Button("Add") {
                let newEntry = Entry (name: name,email: email, jobPosition: jobPosition)
                entryList.append(newEntry)
                name = ""
                email = ""
                jobPosition = ""
            }
           
            
            //                                // Clear the entryList
            //                                entryList.removeAll()
            
            
        }
        .padding()
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
